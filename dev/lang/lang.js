
MTLG.lang.define({
  'en': {
    'intro' : 'Use the input field below to give your input. Afterwards, press the send button to send the input to the game.',
    'send': 'Send',
    'input_placeholder': 'Type input and press send',
    'title': 'MTLG Input for your game',
    'inputReceived': 'The input was received by the game. You can continue to send inputs or simply close this window, if you are finished.',
    'inputNotReceived': 'Something went wrong. The input was NOT received by the game within the last 10 seconds.',
    'message': 'Message',
    'inputlabel': 'Input',
  },
  'de': {
    'intro' : 'Nutze das untenstehende Inputfeld für deine Eingabe. Drück anschließend den Senden-Button, um die Eingabe an das Spiel zu senden.',
    'send': 'Senden',
    'input_placeholder': 'Tippe die Eingabe und drück Senden',
    'title': 'MTLG Eingabe für dein Spiel',
    'inputReceived': 'Die Eingabe wurde vom Spiel empfangen. Du kannst weiter Eingaben senden oder einfach das Fenster schließen, wenn du fertig bist.',
    'inputReceived': 'Etwas ist schief gelaufen. Die Eingabe wurde NICHT innerhalb der letzten 10 Sekunden vom Spiel empfangen.',
    'message': 'Nachricht',
    'inputlabel': 'Eingabe',
  }
});
