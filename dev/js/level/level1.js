/**
 * @Date:   2020-03-18T11:42:01+01:00
 * @Last modified time: 2020-03-18T13:09:23+01:00
 */



function firstlevel_init() {
  // initialize level 1
  MTLG.distributedDisplays.rooms.createRoom('inputKeyboards', function(result) {
    if (result && result.success) {
      drawLevel1();
    } else {
      // This client is a slave. Now it tries to join the room
      MTLG.distributedDisplays.rooms.joinRoom('inputKeyboards', function(result) {
        if (result && result.success) {
          drawLevel1();
        }
      });
    }
  });
}

// check wether level 1 is choosen or not
function checkLevel1(gameState) {
  if (!gameState || (gameState.nextLevel && gameState.nextLevel == "level1")) {
    return 1;
  }
  return 0;
}


function drawLevel1() {
  // add css
  var csslink = document.createElement('link');
  csslink.setAttribute('rel', 'stylesheet');
  csslink.setAttribute('href', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');

  document.head.appendChild(csslink);


  // delete canvas
  var background = document.getElementById('background');
  background.parentNode.removeChild(background)

  var canvasObject = document.getElementById('canvasObject');
  canvasObject.parentNode.removeChild(canvasObject);

  var menu = document.getElementById('menu');
  menu.parentNode.removeChild(menu);

  document.body.style.background = "white";

  // create new dom structure
  var div = document.createElement("div");
  div.setAttribute("class", "container");

  var title = document.createElement("h1");
  title.innerText = MTLG.lang.getString('title');
  div.appendChild(title);

  var p = document.createElement("p");
  p.innerText = MTLG.lang.getString('intro');
  div.appendChild(p);

  // form
  var divForm = document.createElement("div");
  divForm.setAttribute('class', 'form-group');

  // label
  var label = document.createElement("label");
  label.setAttribute('for', 'inputForGame');
  label.innerText = MTLG.lang.getString('inputlabel');
  divForm.appendChild(label);

  // input
  var input = document.createElement("input");
  input.setAttribute('id', 'inputForGame');
  input.setAttribute('type', 'text');
  input.setAttribute('placeholder', MTLG.lang.getString('input_placeholder'));
  input.setAttribute('class', 'form-control');
  divForm.appendChild(input);
  div.appendChild(divForm);

  // send button
  var send = document.createElement("input")
  send.setAttribute("type", "button");
  send.setAttribute("value", MTLG.lang.getString("send"));
  send.setAttribute("class", "btn btn-primary");
  let timeoutRef;
  send.onclick = function() {
    var identifier = null;
    if (location.search.indexOf('identifier') !== -1) {
      var params = parse_query_string(window.location.search.substring(1));
      identifier = params['identifier'];
    }

    var text = input.value;
    MTLG.distributedDisplays.communication.sendCustomAction("inputKeyboards", "getInput", identifier, text);
    MTLG.distributedDisplays.actions.setCustomFunction("inputReceived", onInputReceived);

    timeoutRef = window.setTimeout(onInputReceived, 10000, false);
  };

  var onInputReceived = function(received) {
    window.clearTimeout(timeoutRef);
    MTLG.distributedDisplays.actions.removeCustomFunction("inputReceived", onInputReceived);


    var info = document.createElement("div");
    info.setAttribute('role', 'alert');
    if(received) {
      info.setAttribute("class", "alert alert-success");
      info.innerHTML = MTLG.lang.getString('inputReceived') + "<br>" + MTLG.lang.getString('message')  + ": " + input.value;
      input.value = "";
    } else {
      info.setAttribute("class", "alert alert-danger");
      info.innerHTML = MTLG.lang.getString('inputNotReceived') + "<br>" + MTLG.lang.getString('message')  + ": " + input.value;
    }
    div.appendChild(info);

    window.setTimeout(() => {div.removeChild(info)}, 5000);
  };

  div.appendChild(send);

  document.body.appendChild(div);
}


var parse_query_string = function(query) {
  var vars = query.split("&");
  var query_string = {};
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    var key = decodeURIComponent(pair[0]);
    var value = decodeURIComponent(pair[1]);
    // If first entry with this name
    if (typeof query_string[key] === "undefined") {
      query_string[key] = decodeURIComponent(value);
    }
  }
  return query_string;
}
